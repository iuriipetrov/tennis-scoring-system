package com.ypdev.tennis;

public class Match {
	
	private String playerOne;
	private String playerTwo;
	
	private Set set;
	private Game game;
	
	public Match(String playerOne, String playerTwo) {
		super();
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.game = new Game(playerOne, playerTwo);
		this.set = new Set(playerOne, playerTwo);
	}

	public void pointWonBy(String player) {
		if (this.game.isFinished()) {
			this.finishGame();
		}
		this.game.addScore(player);
	}
	
	public String score() {
		String result = ", " + this.getGameScore();
		if (this.game.isFinished()) {
			this.finishGame();
		}
		result = this.getSetScore() + result;
		if (this.set.isFinished()) {
			String setWinner = this.set.isWonBy(playerOne) ? playerOne : playerTwo;
			result = result + ", set " + setWinner;
			this.set.clear();
			
		}
		return result;
	}
	
	protected String getGameScore() {
		return this.game.toString();
	}
	
	protected String getSetScore() {
		return this.set.toString();
	}
	
	
	private void finishGame() {
		if (this.game.isWonBy(playerOne)) {
			this.game.clear();
			this.set.gameWonBy(playerOne);
		} 
		if (this.game.isWonBy(playerTwo)) {
			this.game.clear();
			this.set.gameWonBy(playerTwo);
		}
		if (this.set.isTieBreak()) {
			this.game.setTieBreak(true);
		}
	}
	
}

