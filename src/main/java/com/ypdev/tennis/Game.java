package com.ypdev.tennis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {
	
	private static final String INITIAL_SCORE = "0-0";
	private static final String GAME = "game";
	private static final String DEUCE = "deuce";
	private static final String ADVANTAGE = "advantage";
	private final Map<Integer, String> TSCORES = new HashMap<Integer, String>();	
	private List<String> scores = new ArrayList<>();
	private String playerOne;
	private String playerTwo;
	private boolean tieBreak = false;

	public Game(String playerOne, String playerTwo) {
		TSCORES.put(0, "0");
		TSCORES.put(1, "15");
		TSCORES.put(2, "30");
		TSCORES.put(3, "40");
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
	}

	/**
	 * Add a winning score to a player
	 * @param player
	 */
	public void addScore(String player) {
		this.scores.add(player);
	}
	
	/**
	 * Reset current scores
	 */
	public void clear() {
		this.scores.clear();
	}
	
	/**
	 * Get score of a player
	 * @param player
	 * @return Score of a player
	 */
	public int getScore(String player) {
		return scores.stream().filter(e->e.contentEquals(player)).collect(Collectors.toList()).size();
	}
	
	/**
	 * Check if Game was won by one of the players
	 * @return true if game finished 
	 */
	public boolean isFinished() {
		return this.isWonBy(this.playerOne) || this.isWonBy(this.playerTwo);
	}
	
	/**
	 * Check if a player won the game
	 * @param player
	 * @return true if the player won the game
	 */
	public boolean isWonBy(String player) {
		int psOne = this.getScore(player);
		int psTwo = this.scores.size()-psOne;
		boolean result = psOne > 3 && psOne-psTwo >= 2;
		if (this.isTieBreak()) {
			result = psOne > 5 && psOne-psTwo >= 2;
		}
		return result; 
	}
	
	public boolean isTieBreak() {
		return this.tieBreak;
	}

	public void setTieBreak(boolean tieBreak) {
		this.tieBreak = tieBreak;
	}

	@Override
	public String toString() {
		if (scores.size() < 1) return INITIAL_SCORE;
		int psOne = this.getScore(this.playerOne);
		int psTwo = Math.abs(this.scores.size()-psOne);
		if (this.isTieBreak()) {
			return psOne+"-"+psTwo; 
		} else {
			return this.scoreToString(psOne, psTwo);
		}
	}

	private String scoreToString(int psOne, int psTwo) {
		String result = INITIAL_SCORE;
		if (this.isWonBy(this.playerOne)) {
			result = GAME + " " + this.playerOne;
		} else if (this.isWonBy(this.playerTwo)) {
			result = GAME + " " + this.playerTwo;
		} else if (psOne > 2 && psTwo > 2) {
			if (psOne == psTwo) result = DEUCE;
			if (psOne > psTwo) result = ADVANTAGE + " " + this.playerOne;
			if (psTwo > psOne) result = ADVANTAGE + " " + this.playerTwo;
		} else {
			result = TSCORES.get(psOne)+"-"+TSCORES.get(psTwo);
		}
		return result;
	}
	
}
