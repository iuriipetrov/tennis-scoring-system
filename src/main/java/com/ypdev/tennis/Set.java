package com.ypdev.tennis;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Set {
	
	private static final String INITIAL_SCORE = "0-0";
	private List<String> games = new ArrayList<>();
	private String playerOne;
	private String playerTwo;
	private boolean tieBreak = false;

	public Set(String playerOne, String playerTwo) {
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
	}

	/**
	 * A player won the game
	 * @param player
	 */
	public void gameWonBy(String player) {
		games.add(player);
		int psOne = this.getScore(player);
		int psTwo = this.games.size()-psOne;
		if (psOne > 5 && psTwo > 5) {
			tieBreak = true;
		}
	}

	private int getScore(String player) {
		return games.stream().filter(e->e.contentEquals(player)).collect(Collectors.toList()).size();
	}

	public boolean isFinished() {
		return this.isWonBy(this.playerOne) || this.isWonBy(this.playerTwo);
	}
	
	public boolean isTieBreak() {
		return this.tieBreak;
	}
	
	public boolean isWonBy(String player) {
		int psOne = this.getScore(player);
		int psTwo = this.games.size()-psOne;
		boolean result = psOne > 5 && psOne-psTwo >= 2;
		if (this.tieBreak) {
			result = psOne > 6;
		}
		return result; 
	}
	
	@Override
	public String toString() {
		if (games.size() < 1) return INITIAL_SCORE;
		int psOne = this.getScore(playerOne);
		int psTwo = this.getScore(playerTwo);
		return psOne+"-"+psTwo;
	}

	public void clear() {
		this.games.clear();
	}

}
