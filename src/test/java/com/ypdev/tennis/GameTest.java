package com.ypdev.tennis;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ypdev.tennis.Game;

public class GameTest {
	
	@Test
	public void testPlayerOneWonGameAfter40to0() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p1");
		assertEquals("game p1", game.toString());
		assertTrue(game.isFinished());
		assertTrue(game.isWonBy("p1"));
	}
	
	@Test
	public void testPlayerOneWonGameAfter40to15() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p1");
		assertEquals("game p1", game.toString());
		assertTrue(game.isFinished());
		assertTrue(game.isWonBy("p1"));
	}
	
	@Test
	public void testPlayerOneWonGameAfter40to30() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p1");
		assertEquals("game p1", game.toString());
		assertTrue(game.isFinished());
		assertTrue(game.isWonBy("p1"));
	}
	
	@Test
	public void testPlayerOnePlayerTwoDuece() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p2");
		assertEquals("deuce", game.toString());
		assertFalse(game.isFinished());
	}
	
	@Test
	public void testPlayerOneAdvantage() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p2");
		assertEquals("deuce", game.toString());
		game.addScore("p1");
		assertEquals("advantage p1", game.toString());
		assertFalse(game.isFinished());
	}
	
	@Test
	public void testPlayerTwoAdvantage() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p2");
		assertEquals("deuce", game.toString());
		game.addScore("p2");
		assertEquals("advantage p2", game.toString());
		assertFalse(game.isFinished());
	}
	
	@Test
	public void testPlayerOneWonAfterAdvantage() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p2");
		assertEquals("deuce", game.toString());
		game.addScore("p1");
		assertEquals("advantage p1", game.toString());
		game.addScore("p1");
		assertEquals("game p1", game.toString());
		assertTrue(game.isFinished());
		assertTrue(game.isWonBy("p1"));
	}
	
	@Test
	public void testPlayerTwoWonAfterAdvantage() {
		Game game = new Game("p1", "p2");
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("15-0", game.toString());
		game.addScore("p1");
		assertEquals("30-0", game.toString());
		game.addScore("p1");
		assertEquals("40-0", game.toString());
		game.addScore("p2");
		assertEquals("40-15", game.toString());
		game.addScore("p2");
		assertEquals("40-30", game.toString());
		game.addScore("p2");
		assertEquals("deuce", game.toString());
		game.addScore("p2");
		assertEquals("advantage p2", game.toString());
		game.addScore("p2");
		assertEquals("game p2", game.toString());
		assertTrue(game.isFinished());
		assertTrue(game.isWonBy("p2"));
	}
	
	@Test
	public void testTieBreakIsntFinishedYet5to6() {
		Game game = new Game("p1", "p2");
		game.setTieBreak(true);
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("1-0", game.toString());
		game.addScore("p1");
		assertEquals("2-0", game.toString());
		game.addScore("p1");
		assertEquals("3-0", game.toString());
		game.addScore("p2");
		assertEquals("3-1", game.toString());
		game.addScore("p2");
		assertEquals("3-2", game.toString());
		game.addScore("p2");
		assertEquals("3-3", game.toString());
		game.addScore("p2");
		assertEquals("3-4", game.toString());
		game.addScore("p2");
		assertEquals("3-5", game.toString());
		game.addScore("p1");
		game.addScore("p1");
		assertFalse(game.isFinished());
		game.addScore("p2");
		assertEquals("5-6", game.toString());
		assertFalse(game.isFinished());
	}
	
	@Test
	public void testTieBreakIsntFinishedYet8to7() {
		Game game = new Game("p1", "p2");
		game.setTieBreak(true);
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("1-0", game.toString());
		game.addScore("p1");
		assertEquals("2-0", game.toString());
		game.addScore("p1");
		assertEquals("3-0", game.toString());
		game.addScore("p2");
		assertEquals("3-1", game.toString());
		game.addScore("p2");
		assertEquals("3-2", game.toString());
		game.addScore("p2");
		assertEquals("3-3", game.toString());
		game.addScore("p2");
		assertEquals("3-4", game.toString());
		game.addScore("p2");
		assertEquals("3-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("4-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("5-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p2");
		assertEquals("5-6", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("6-6", game.toString());
		game.addScore("p1");
		assertEquals("7-6", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p2");
		assertEquals("7-7", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("8-7", game.toString());
		assertFalse(game.isFinished());
	}
	
	@Test
	public void testPlayerOneWonTieBreak() {
		Game game = new Game("p1", "p2");
		game.setTieBreak(true);
		assertEquals("0-0", game.toString());
		game.addScore("p1");
		assertEquals("1-0", game.toString());
		game.addScore("p1");
		assertEquals("2-0", game.toString());
		game.addScore("p1");
		assertEquals("3-0", game.toString());
		game.addScore("p2");
		assertEquals("3-1", game.toString());
		game.addScore("p2");
		assertEquals("3-2", game.toString());
		game.addScore("p2");
		assertEquals("3-3", game.toString());
		game.addScore("p2");
		assertEquals("3-4", game.toString());
		game.addScore("p2");
		assertEquals("3-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("4-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("5-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("6-5", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p2");
		assertEquals("6-6", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("7-6", game.toString());
		assertFalse(game.isFinished());
		game.addScore("p1");
		assertEquals("8-6", game.toString());
		assertTrue(game.isFinished());		
		assertTrue(game.isWonBy("p1"));
	}

}
