package com.ypdev.tennis;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ypdev.tennis.Set;

public class SetTest {

	@Test
	public void testPlayerOneWonOneGameInSet() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertEquals("1-0", set.toString());
	}

	@Test
	public void testPlayerOneWonSet6to4() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertEquals("6-4", set.toString());
		assertTrue(set.isFinished());
	}
	
	@Test
	public void testPlayerOneIsntWonSetYet6to5() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertEquals("6-5", set.toString());
		assertFalse(set.isFinished());
	}
	
	@Test
	public void testPlayerOneIsntWonSet7to5() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertEquals("6-5", set.toString());
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertEquals("7-5", set.toString());
		assertTrue(set.isFinished());
	}
	
	@Test
	public void testPlayersGotInTieBreak() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertEquals("6-6", set.toString());
		assertFalse(set.isFinished());
		assertTrue(set.isTieBreak());
		
	}
	
	@Test
	public void testPlayersOneAfterTieBreak() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertEquals("6-6", set.toString());
		assertFalse(set.isFinished());
		assertTrue(set.isTieBreak());
		set.gameWonBy("p1");
		assertTrue(set.isFinished());
		assertEquals("7-6", set.toString());
	}
	
	@Test
	public void testPlayersTwoAfterTieBreak() {
		Set set = new Set("p1", "p2");
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertFalse(set.isFinished());
		set.gameWonBy("p1");
		assertFalse(set.isFinished());
		set.gameWonBy("p2");
		assertEquals("6-6", set.toString());
		assertFalse(set.isFinished());
		assertTrue(set.isTieBreak());
		set.gameWonBy("p2");
		assertTrue(set.isFinished());
		assertEquals("6-7", set.toString());
	}
	

}
