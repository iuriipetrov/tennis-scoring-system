package com.ypdev.tennis;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ypdev.tennis.Match;

public class MatchTest {

	@Test
	public void testNadavVsFederer2008() {
		String p1 = "Federer";
		String p2 = "Nadal";
		Match match = new Match(p1, p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p2);
		match.pointWonBy(p1);
		match.pointWonBy(p1);
		assertEquals("6-6, 9-8", match.score());
		match.pointWonBy(p1);
		assertEquals("7-6, 10-8, set Federer", match.score());
	}

}
